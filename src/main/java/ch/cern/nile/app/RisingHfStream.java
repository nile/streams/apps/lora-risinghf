package ch.cern.nile.app;

import ch.cern.nile.app.generated.RisingHfPacket;
import ch.cern.nile.kaitai.streams.LoraDecoderStream;

public final class RisingHfStream extends LoraDecoderStream<RisingHfPacket> {

    public RisingHfStream() {
        super(RisingHfPacket.class);
    }

}
