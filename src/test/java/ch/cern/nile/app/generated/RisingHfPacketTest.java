package ch.cern.nile.app.generated;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import com.google.gson.JsonElement;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.exceptions.DecodingException;
import ch.cern.nile.kaitai.decoder.KaitaiPacketDecoder;
import ch.cern.nile.test.utils.TestUtils;

class RisingHfPacketTest {

    private static final JsonElement DATA_FRAME = TestUtils.getDataAsJsonElement("AWxonTAAkCnI");

    @Test
    void givenDataFrame_whenDecoding_thenCorrectlyDecodesMessageData() throws DecodingException {
        final Map<String, Object> packet = KaitaiPacketDecoder.decode(DATA_FRAME, RisingHfPacket.class);

        assertEquals(1, packet.get("header"));
        assertEquals(24.82582763671875, packet.get("temperature"));
        assertEquals(70.66015625, packet.get("humidity"));
        assertEquals(96.0, packet.get("period"));
        assertEquals(-36.0, packet.get("rssi"));
        assertEquals(10.25, packet.get("snr"));
        assertEquals(3.5, packet.get("battery"));
    }

}
