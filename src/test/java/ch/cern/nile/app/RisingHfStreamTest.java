package ch.cern.nile.app;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonObject;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;

import ch.cern.nile.test.utils.StreamTestBase;
import ch.cern.nile.test.utils.TestUtils;

class RisingHfStreamTest extends StreamTestBase {

    private static final JsonObject FRAME_DATA = TestUtils.loadRecordAsJson("data/frame_data.json");

    @Override
    public RisingHfStream createStreamInstance() {
        return new RisingHfStream();
    }

    @Test
    void givenDataFrame_whenDecoding_thenCorrectOutputRecordIsCreated() {
        pipeRecord(FRAME_DATA);
        final ProducerRecord<String, JsonObject> outputRecord = readRecord();

        assertEquals(1, outputRecord.value().get("header").getAsInt());
        assertEquals((float) 24.825829, outputRecord.value().get("temperature").getAsFloat());
        assertEquals((float) 70.66016, outputRecord.value().get("humidity").getAsFloat());
        assertEquals((float) 96.0, outputRecord.value().get("period").getAsFloat());
        assertEquals((float) -36.0, outputRecord.value().get("rssi").getAsFloat());
        assertEquals((float) 10.25, outputRecord.value().get("snr").getAsFloat());
        assertEquals((float) 3.5, outputRecord.value().get("battery").getAsFloat());
        assertNotNull(outputRecord.value().get("timestamp"));
    }

}
